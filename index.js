const paragraphs = document.querySelectorAll('p');
paragraphs.forEach((p) => {p.style.backgroundColor = '#ff0000'});

const elementId = document.getElementById('optionsList');
console.log(elementId);

const parentElement = elementId.parentNode;
console.log(parentElement);

if (elementId.hasChildNodes()) {
    const child = elementId.childNodes;
    for (let i = 0; i < child.length; ++i) {
      console.log(`Node ${i + 1} - name: ${elementId.childNodes[i].nodeName}, type: ${elementId.childNodes[i].nodeType}`);
    }
}

const elementClassName = document.getElementsByClassName('testParagraph');
console.log(elementClassName);

const newElement = document.getElementById('testParagraph');
newElement.innerText = 'This is a paragraph';
console.log(newElement);

let mainHeaderLi = document.querySelector('.main-header').querySelectorAll('li');
console.log(mainHeaderLi);
for (let i = 0; i < mainHeaderLi.length; i++) {
  mainHeaderLi[i].className = 'nav-item'
}
console.log(mainHeaderLi);

const elementSectionTitle = document.querySelectorAll('.section-title');
for (let elementRemove of elementSectionTitle) {
  elementRemove.classList.remove('section-title')
}